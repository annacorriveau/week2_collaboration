# Library of functions
import scipy.stats as stats
import numpy as np

def dprime(hits, fas, misses, crs):
    """
    Function to calculate d' 
    :hits: number of hits
    :fas: number of false alarms
    :misses: number of misses
    :crs: number of correct rejections
    :dprime: returns dprime (sensitivity) value
    """
   # import numpy
    

    HR = hits/(hits+misses)
    FA = fas/(crs+fas)

    # Calculate d' for memory task
    HR_Z =  stats.norm.ppf(HR)
    FA_Z =  stats.norm.ppf(FA)

    dPrime = HR_Z-FA_Z
    return dPrime



def preRT(df):
    """
    Function to calculate d' 
    :hits: number of hits
    :fas: number of false alarms
    :misses: number of misses
    :crs: number of correct rejections
    :dprime: returns dprime (sensitivity) value
    """
    x = df['role_aud']
    if x[5][0] == 'r':
        rel_col_name = 'Audio_freqinfreq'
    elif  x[5][0] == 'i':  
        rel_col_name = 'Visual_freqinfreq'
    else:
        print('ERROR- unsure whether this ppt attended to visual or auditory stimulus')
    print('Ppt attended to ' + rel_col_name[0:3] + ' Stimuli')
    
    # Define columns needed for analysis
    #IMPORTANT - freqinfreq columns display previous trial info- so index -1 when counting preceding trials
    rel_col = df[rel_col_name]
    acc = df['accuracy']
    hits = df['hit']
    misses = df['miss']
    CRs = df['CR']
    FAs = df['FA']
    rt = df['key_resp.rt']
    rt[1:30]
    print(type(rt[28]))
    print(type(rt[29]))
    
    # loop through relevant column and find where target stim were presented
    corr_ct = -1
    fa_ct = -1

    rt_preceding_correct = []
    rt_preceding_incorrect = []

    for tr, trial in enumerate(rel_col):
        if trial == 1 and tr > 3:          # find where targets were displayed
            if acc[tr] == 1:               # if ppt got trial correct
                corr_ct = corr_ct + 1      # count number of correct rejections
                rt_add = []
                if hits[tr-1] == 1 and isinstance(rt[tr-2], str):
                    rt1 = float(rt[tr-2].strip("[]"))
                    rt_add.append(rt1)
                if hits[tr-2] == 1 and isinstance(rt[tr-3], str):
                    rt2 = float(rt[tr-3].strip("[]"))
                    rt_add.append(rt2)
                if hits[tr-3] == 1 and isinstance(rt[tr-4], str):
                    rt3 = float(rt[tr-4].strip("[]"))
                    rt_add.append(rt3)
                rt_preceding_correct.append(stats.mean(rt_add)) 
            elif acc[tr] == 0:             # if ppt had FA on trial
                fa_ct = fa_ct + 1
                rt_add = []
                if hits[tr-1] == 1 and isinstance(rt[tr-2], str):
                    rt1 = float(rt[tr-2].strip("[]"))
                    rt_add.append(rt1)
                if hits[tr-2] == 1 and isinstance(rt[tr-3], str):
                    rt2 = float(rt[tr-3].strip("[]"))
                    rt_add.append(rt2)
                if hits[tr-3] == 1 and isinstance(rt[tr-4], str):
                    rt3 = float(rt[tr-4].strip("[]"))
                    rt_add.append(rt3)
                rt_preceding_incorrect.append(stats.mean(rt_add))            
            
    mean_rt_preceding_correct = stats.mean(rt_preceding_correct)
    print('rt correct', mean_rt_preceding_correct)

    mean_rt_preceding_incorrect = stats.mean(rt_preceding_incorrect)
    print('rt incorrect', mean_rt_preceding_incorrect)

    print(corr_ct)
    print(fa_ct)
    return(mean_rt_preceding_correct, mean_rt_preceding_incorrect)