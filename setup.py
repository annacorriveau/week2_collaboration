import setuptools

setuptools.setup(
    name="Week2_collaboration",
    version="0.1.0",
    author="Anna Corriveau",
    author_email="corriveaual@uchicago.edu",
    description="package setup practice for PSYC 40650",
    url="https://bitbucket.org/annacorriveau/week2_collaboration/",
    packages=setuptools.find_packages(),
    install_requires=[
        'pandas',
        'matplotlib',
        'numpy',
        'scipy',
        'scikit-learn',
        'bokeh',
        'seaborn'
    ],
)
